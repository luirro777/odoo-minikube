# odoo-minikube

Conjunto de manifiestos que permiten levantar una instancia de odoo via minikube

## Pasos

### 1 - Instalar minikube

### 2 - Inicializar el cluster

`minikube start`

### 3 - Correr script de despliegue

`./deploy.sh`

### 4 - Levantar la instancia

`minikube service odoo-service  `