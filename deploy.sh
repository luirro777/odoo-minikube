#!/bin/bash

#kubectl apply -f pv-pvc.yaml 
kubectl apply -f pv-pvc-nfs.yaml 
kubectl apply -f postgres-deployment.yaml 
kubectl apply -f postgres-service.yaml

sleep 5

kubectl apply -f odoo-deployment.yaml 
kubectl apply -f odoo-service.yaml 

exit 0